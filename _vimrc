set encoding=utf-8
scriptencoding utf-8

" options {{{
" 1 important
" set rtp=...
silent! vunmap <C-x>
set pastetoggle=<F9>


" 2 moving around, searching and patterns
set whichwrap=b,s
" set startofline
" set noautochdir
set wrapscan
set incsearch " Ctrl-G, Ctrl-T
" set magic
set ignorecase smartcase

" 3 tags
set tags& tags+=tags;

" 4 displaying text
set wrap linebreak breakindent
set scrolloff=4 sidescroll=4 sidescrolloff=4
set display=lastline
set cmdheight=2
set list listchars=tab:»\ ,eol:$
set number

" 5 syntax, highlighting and spelling
set background=dark
set hlsearch
set colorcolumn=+1
syntax sync minlines=500 maxlines=1000

" 6 multiple windows
set rulerformat=%45(%12f%=\ %m%{'['.(&fenc!=''?&fenc:&enc).']'}\ %l-%v\ %p%%\ %)
set statusline=%f:\ %{substitute(getcwd(),'.*/','','')}\ %m%=%{(&fenc!=''?&fenc:&enc).':'.&ff}\ %3l-%-2v\ %p%%\ 
set hidden

" 7 multiple tab pages
set showtabline=2

" 8 terminal
" 9 using the mouse
set mouse=a

" 10 GUI
" 11 printing
" 12 messages and info
set shortmess+=aI
set showcmd
set showmode
set ruler
set noerrorbells
set visualbell t_vb=

" 13 selecting text
" 14 editing text
set undofile
set undolevels=1000
set undodir=~/var/vim/undo
set completeopt=menu,preview
set pumheight=10
set showmatch matchtime=5
set backspace=indent,eol,start
set formatoptions-=ro

" 15 tabs and indenting
set tabstop=4 shiftwidth=4 softtabstop=4 noexpandtab
set autoindent smartindent smarttab
set cinoptions& cinoptions+=(s,m1,t0,E-s
set cinoptions+=g0,N-s

" 16 folding
" 17 diff mode
" 18 mapping
" 19 reading and writing files
set autoread
set backup
set backupdir=~/var/vim/bkup

" 20 the swap file
set swapfile
set directory=~/var/vim/swap
set updatetime=2000

" 21 command line editing
set fileignorecase
set wildignorecase
set wildmenu
set cmdwinheight=7

" 22 executing external commands
" 23 running make and jumping to errors
" 24 system specific
set noshellslash

" 25 language specific
set iminsert=0 imsearch=0

" 26 multi-byte characters
set fileencodings=cp932,euc-jp,utf-8
augroup au_custom_encs
  autocmd!
  autocmd BufReadPre,BufNewFile *.INI     set fencs^=ucs-bom,ucs-2le
  autocmd BufRead *.INI                   set fencs-=ucs-bom,ucs-2le
augroup END


" 27 various
" set gdefault
" more old files
let &viminfo = substitute(&viminfo, "'\\zs\\d*", '500', '')
" set viminfo+=n~/var/vim/vinfo
" set luadll=lua51.dll
" set perldll=perl524.dll
" set pythondll=python27.dll
" set pythonthreedll=python35.dll
" set rubydll=x64-msvcrt-ruby230.dll
set sessionoptions=buffers,curdir,folds,tabpages,winsize
cd ~
" }}}

" mappings {{{
nnoremap j gj
nnoremap k gk
nnoremap Y y$
nnoremap n nzv
nnoremap N Nzv
nnoremap * *zv
nnoremap # #zv
nnoremap <silent> <C-l> <C-l>:noh<CR>
" }}}

" filetypes {{{
augroup au_ft_conf
  autocmd!
  autocmd FileType vim setlocal ts=8 sw=2 sts=2 tw=0 et ai si
  autocmd FileType c,cpp,java setlocal mps+==:; cc+=20,40
  autocmd FileType c,cpp,java setlocal ts=4 sw=4 sts=0 tw=0 noet ai si cin
  autocmd FileType help,qf nnoremap <buffer> q <C-w>c
augroup END

augroup au_cursor
  autocmd!
  autocmd WinLeave * set nocursorline
  autocmd WinEnter * set cursorline
augroup END
" }}}

" commands {{{
" GetHighlightingGroup {{{
command! -nargs=0 GetHighlightingGroup echo 'hi<' . synIDattr(synID(line('.'),col('.'),1),'name') . '> trans<' . synIDattr(synID(line('.'),col('.'),0),'name') . '> lo<' . synIDattr(synIDtrans(synID(line('.'),col('.'),1)),'name') . '>'
" }}}

" syntax_info {{{
function! s:get_syn_id(transparent) 
  let synid = synID(line("."), col("."), 1)
  if a:transparent
    return synIDtrans(synid)
  else
    return synid
  endif
endfunction
function! s:get_syn_attr(synid)
  let name = synIDattr(a:synid, "name")
  let ctermfg = synIDattr(a:synid, "fg", "cterm")
  let ctermbg = synIDattr(a:synid, "bg", "cterm")
  let guifg = synIDattr(a:synid, "fg", "gui")
  let guibg = synIDattr(a:synid, "bg", "gui")
  return {
        \ "name": name,
        \ "ctermfg": ctermfg,
        \ "ctermbg": ctermbg,
        \ "guifg": guifg,
        \ "guibg": guibg}
endfunction
function! s:get_syn_info()
  let baseSyn = s:get_syn_attr(s:get_syn_id(0))
  echo "name: " . baseSyn.name .
        \ " ctermfg: " . baseSyn.ctermfg .
        \ " ctermbg: " . baseSyn.ctermbg .
        \ " guifg: " . baseSyn.guifg .
        \ " guibg: " . baseSyn.guibg
  let linkedSyn = s:get_syn_attr(s:get_syn_id(1))
  echo "link to"
  echo "name: " . linkedSyn.name .
        \ " ctermfg: " . linkedSyn.ctermfg .
        \ " ctermbg: " . linkedSyn.ctermbg .
        \ " guifg: " . linkedSyn.guifg .
        \ " guibg: " . linkedSyn.guibg
endfunction
command! SyntaxInfo call s:get_syn_info()
" }}}

" no_italic {{{
function! s:no_italic()
  if exists("*execute")
    for hi_grp in ["Comment", "Todo", "Folded", "Special"]
      let l:hi = execute("hi " . hi_grp, "silent!")
      let l:hi = substitute(l:hi, ",\\=italic,\\=", "", "g")
      let l:hi = substitute(l:hi, "\\(term\\|gui\\)=\\s\\+", "\\1=none ", "g")
      let l:hi = substitute(l:hi, ".*xxx", "", "g")
      call execute("hi ". hi_grp . l:hi, "silent!")
    endfor
  endif
endfunction
command! NoItalic call s:no_italic()
" }}}
" }}}

" augroup {{{
augroup au_no_italic
 autocmd!
 autocmd ColorScheme * call s:no_italic()
augroup END
" }}}

" loaded_plugins {{{
let g:loaded_getscriptPlugin = 1
let g:loaded_logiPat = 1
let g:loaded_matchparen = 1
let g:loaded_netrw = 1
let g:loaded_netrwPlugin = 1
let g:loaded_vimballPlugin = 1
" }}}

" @vim-plug {{{
let g:plugged_dir='~/vimfiles/plugged'
let g:ll_colo_dir=g:plugged_dir . '/lightline.vim/autoload/lightline/colorscheme/'

call plug#begin(g:plugged_dir)
" plugins {{{
Plug 'Shougo/denite.nvim', {'on': 'Denite'}
Plug 'ctrlpvim/ctrlp.vim'
Plug 'thinca/vim-quickrun'
Plug 'itchyny/lightline.vim'
Plug 'itchyny/vim-cursorword'
Plug 'itchyny/vim-parenmatch'
Plug 'tyru/open-browser.vim'
Plug 'osyo-manga/vim-over'
Plug 'osyo-manga/vim-anzu'
Plug 't9md/vim-quickhl', {'on': '<Plug>(quickhl-'}
Plug 't9md/vim-choosewin', {'on': '<Plug>(choosewin)'}
Plug 'vim-syntastic/syntastic'
Plug 'AndrewRadev/switch.vim'
Plug 'kana/vim-submode'
Plug 'pavoljuhas/oldfilesearch.vim'
Plug 'majutsushi/tagbar'
" Plug 'majutsushi/tagbar', {'on': ['Tagbar', 'TagbarToggle']}
Plug 'tyru/caw.vim'
" Plug 'tyru/caw.vim', {'on': '<Plug>(caw:'}
Plug 'justinmk/vim-dirvish'
" Plug 'justinmk/vim-dirvish', {'on': ['Dirvish', '<Plug>(dirvish_']}
" Plug 'cocopon/vaffle.vim'
Plug 'sjl/gundo.vim', {'on': ['GundoToggle', 'GundoShow']}
Plug 'deris/vim-shot-f'
" }}}

" for {{{
Plug 'cespare/vim-toml', { 'for': 'toml'}
Plug 'davidhalter/jedi-vim', { 'for': 'python'}
Plug 'vimperator/vimperator.vim', { 'for': 'vimperator'}
Plug 'kannokanno/previm', {'for': 'markdown'}
" }}}

" colorscheme {{{
function! CopyLlColos(info) " {{{
"  if a:info.status == 'installed' || a:info.status == 'updated'
    let l:src = 'autoload/lightline/colorscheme/' . a:info.name
    let l:dest = expand(g:ll_colo_dir . a:info.name)
    call writefile(readfile(l:src, 'b'), l:dest, 'b')
"  endif
endfunction " }}}
Plug 'crusoexia/vim-monokai'
Plug 'scwood/vim-hybrid'
Plug 'tyrannicaltoucan/vim-quantum'
Plug 'rakr/vim-one'
Plug 'nanotech/jellybeans.vim'
Plug 'morhetz/gruvbox', { 'do': function('CopyLlColos'), 'as': 'gruvbox.vim' }
Plug 'cocopon/iceberg.vim', { 'do': function('CopyLlColos') }
" }}}
call plug#end()
" }}}

" @denite {{{
nnoremap [denite] <Nop>
nmap <Space>d [denite]

nnoremap [denite]n :Denite -resume -cursor-pos=+1 -immediately<CR>
nnoremap [denite]p :Denite -resume -cursor-pos=-1 -immediately<CR>
nnoremap [denite]b :Denite buffer<CR>
nnoremap [denite]B :Denite -resume buffer<CR>
nnoremap [denite]f :Denite file_rec<CR>
nnoremap [denite]m :Denite file_mru<CR>

augroup au_denite_user
  autocmd! User denite.nvim call s:denite_custom()
augroup END
function! s:denite_custom()
call denite#custom#map('insert', '<C-a>', '<Home>')
call denite#custom#map('insert', '<C-e>', '<End>')
call denite#custom#map('insert', '<C-f>', '<Right>')
call denite#custom#map('insert', '<C-b>', '<Left>')
call denite#custom#map('insert', '<C-j>', '<denite:move_to_next_line>', 'noremap')
call denite#custom#map('insert', '<C-k>', '<denite:move_to_previous_line>', 'noremap')
call denite#custom#map(
      \ 'insert',
      \ '<C-d>',
      \ '<denite:delete_char_under_caret>',
      \ 'noremap'
      \)
endfunction
" }}}

" @QuickRun {{{
let g:quickrun_config = {}
let g:quickrun_config._ = {
      \ 'outputter/buffer/close_on_empty' : 1
      \ }
command! -nargs=+ -complete=command Capture QuickRun -type vim -src <q-args>
" }}}

" @quickhl {{{
nnoremap [quickhl] <Nop>
nmap <Space>h [quickhl]

nmap [quickhl]h <Plug>(quickhl-manual-this)
xmap [quickhl]h <Plug>(quickhl-manual-this)
nmap [quickhl]l <Plug>(quickhl-manual-toggle)
xmap [quickhl]l <Plug>(quickhl-manual-toggle)
nmap [quickhl]k <Plug>(quickhl-manual-reset)
xmap [quickhl]k <Plug>(quickhl-manual-reset)

nmap [quickhl]j <Plug>(quickhl-cword-toggle)
nmap [quickhl]] <Plug>(quickhl-tag-toggle)
" }}}

" @lightline {{{
set guioptions-=e
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'enable': {
      \   'statusline': 1,
      \   'tabline': 1,
      \ },
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ], [ 'filename' ], ['cur_func', 'ctrlpmark'] ],
      \   'right': [ [ 'syntastic', 'lineall', 'lineinfo' ], ['percent'], [ 'fileformat', 'fileencoding', 'filetype' ] ]
      \ },
      \ 'tabline': {
      \   'left': [ [ 'tabs' ] ],
      \   'right': [ [ 'close' ], ['cwd'] ]
      \ },
      \ 'component': {
      \   'lineall': '%3L'
      \ },
      \ 'component_function': {
      \   'fugitive': 'LightlineFugitive',
      \   'filename': 'LightlineFilename',
      \   'fileformat': 'LightlineFileformat',
      \   'filetype': 'LightlineFiletype',
      \   'fileencoding': 'LightlineFileencoding',
      \   'cwd': 'getcwd',
      \   'mode': 'LightlineMode',
      \   'ctrlpmark': 'CtrlPMark',
      \   'cur_func': 'TagbarCurrent',
      \ },
      \ 'component_expand': {
      \   'syntastic': 'SyntasticStatuslineFlag',
      \ },
      \ 'component_type': {
      \   'syntastic': 'error',
      \ }
      \ }

function! LightlineModified()
  return &ft ==# 'help' ? '' : &modified ? '+' : &modifiable ? '' : '-'
endfunction

function! LightlineReadonly()
  return &ft !=# 'help' && &readonly ? 'RO' : ''
endfunction

function! LightlineFilename()
  let fname = expand('%:t')
  return fname ==# 'ControlP' && has_key(g:lightline, 'ctrlp_item') ? g:lightline.ctrlp_item :
        \ fname =~# '__Gundo' ? '' :
        \ &ft ==# 'tagbar' ? g:lightline.tagb_fname :
        \ &ft ==# 'dirvish' ? expand('%') :
        \ ('' !=# LightlineReadonly() ? LightlineReadonly() . ' ' : '') .
        \ ('' !=# fname ? fname : '[No Name]') .
        \ ('' !=# LightlineModified() ? ' ' . LightlineModified() : '')
endfunction
        " \ &ft == 'vimfiler' ? vimfiler#get_status_string() :
        " \ &ft == 'unite' ? unite#get_status_string() :
        " \ &ft == 'vimshell' ? vimshell#get_status_string() :

function! LightlineFugitive()
  try
    if expand('%:t') !~? 'Tagbar\|Gundo\|NERD' && &ft !~? 'vimfiler' && exists('*fugitive#head')
      let mark = ''  " edit here for cool mark
      let branch = fugitive#head()
      return branch !=# '' ? mark.branch : ''
    endif
  catch
  endtry
  return ''
endfunction

function! LightlineFileformat()
  return winwidth(0) > 70 ? &fileformat : ''
endfunction

function! LightlineFiletype()
  return winwidth(0) > 70 ? (&filetype !=# '' ? &filetype : 'no ft') : ''
endfunction

function! LightlineFileencoding()
  return winwidth(0) > 70 ? (&fenc !=# '' ? &fenc : &enc) : ''
endfunction

function! LightlineMode()
  let fname = expand('%:t')
  return &ft ==# 'tagbar' ? 'Tagbar' :
        \ &ft ==# 'dirvish' ? 'dirvish' :
        \ fname ==# 'ControlP' ? 'CtrlP' :
        \ fname ==# '__Gundo__' ? 'Gundo' :
        \ fname ==# '__Gundo_Preview__' ? 'Gundo Preview' :
        \ winwidth(0) > 60 ? lightline#mode() : ''
endfunction
        " \ fname =~ 'NERD_tree' ? 'NERDTree' :
        " \ fname == '__Tagbar__' ? 'Tagbar' :
        " \ &ft == 'unite' ? 'Unite' :
        " \ &ft == 'vimfiler' ? 'VimFiler' :
        " \ &ft == 'vimshell' ? 'VimShell' :

function! CtrlPMark()
  if expand('%:t') ==# 'ControlP' && has_key(g:lightline, 'ctrlp_item')
    call lightline#link('iR'[g:lightline.ctrlp_regex])
    return lightline#concatenate([g:lightline.ctrlp_prev, g:lightline.ctrlp_item
          \ , g:lightline.ctrlp_next], 0)
  else
    return ''
  endif
endfunction

function! TagbarCurrent()
  return winwidth(0) > 70 ? tagbar#currenttag('%s', '') : ''
endfunction

let g:ctrlp_status_func = {
  \ 'main': 'CtrlPStatusFunc_1',
  \ 'prog': 'CtrlPStatusFunc_2',
  \ }

function! CtrlPStatusFunc_1(focus, byfname, regex, prev, item, next, marked)
  let g:lightline.ctrlp_regex = a:regex
  let g:lightline.ctrlp_prev = a:prev
  let g:lightline.ctrlp_item = a:item
  let g:lightline.ctrlp_next = a:next
  return lightline#statusline(0)
endfunction

function! CtrlPStatusFunc_2(str)
  return lightline#statusline(0)
endfunction

let g:tagbar_status_func = 'TagbarStatusFunc'

function! TagbarStatusFunc(current, sort, fname, ...) abort
    let g:lightline.tagb_fname = a:fname
    let g:lightline.tagb_curtag = tagbar#currenttag('%s', '')
  return lightline#statusline(0)
endfunction

augroup AutoSyntastic
  autocmd!
  autocmd BufWritePost *.vim,*vimrc,*.py call s:syntastic()
augroup END
function! s:syntastic()
  SyntasticCheck
  call lightline#update()
endfunction

function! s:lightline_separator()
  if &guifont =~? 'for *Po'
    let g:lightline.separator = { 'left': '⮀', 'right': '⮂' }
    let g:lightline.subseparator = { 'left': '⮁', 'right': '⮃' }
  else
    let g:lightline.separator = { 'left': '', 'right': '' }
    let g:lightline.subseparator = { 'left': '|', 'right': '|' }
  endif
endfunction

" lightline colorscheme {{{2
augroup ll_colorscheme
  autocmd!
  autocmd VimEnter,ColorScheme * 
        \ call s:lightline_colo() |
        \ call s:lightline_separator() |
        \ call s:lightline_update()
augroup END
function! s:lightline_update()
  if !exists('g:loaded_lightline')
    return
  endif
  try
    call lightline#init()
    call lightline#colorscheme()
    call lightline#update()
  catch
  endtry
endfunction
function! s:lightline_colo()
  if !exists('g:loaded_lightline')
    return
  endif
  if g:colors_name =~# 'jellybeans\|gruvbox\|tender\|iceberg\|nord'
    let g:lightline.colorscheme = g:colors_name
  else
    let g:lightline.colorscheme = 'wombat'
  endif
endfunction
" }}}

" }}}

" @caw {{{
nmap gc <Plug>(caw:prefix)
xmap gc <Plug>(caw:prefix)

nmap <Plug>(caw:prefix)z <Plug>(caw:wrap:toggle)
xmap <Plug>(caw:prefix)z <Plug>(caw:wrap:toggle)
" }}}

" @anzu {{{
nmap n <Plug>(anzu-n-with-echo)
nmap N <Plug>(anzu-N-with-echo)
nmap * <Plug>(anzu-star-with-echo)
nmap # <Plug>(anzu-sharp-with-echo)
augroup my-anzu
  autocmd!
  autocmd WinLeave,TabLeave * call anzu#clear_search_status()
augroup END
" }}}

" @switch {{{
let g:switch_custom_definitions = [
      \   switch#NormalizedCase(['on', 'off']),
      \   switch#NormalizedCase(['yes', 'no']),
      \   switch#NormalizedCase(['true', 'false']),
      \   switch#NormalizedCase(['and', 'or']),
      \ ]
let g:switch_defs_camel_pascal = [
      \   {
      \     '\<[a-z0-9]\+_\k\+\>': {
      \       '_\(.\)': '\U\1'
      \     },
      \     '\<[a-z0-9]\+[A-Z]\k\+\>': {
      \       '\([A-Z]\)': '_\l\1'
      \     },
      \   }
      \ ]
let g:switch_def_quote = [
      \   {
      \       '\(\k\+\)'    : '''\1''',
      \     '''\(.\{-}\)''' :  '"\1"',
      \      '"\(.\{-}\)"'  :   '\1',
      \   }
      \ ]
nnoremap <silent> gss :Switch<CR>
nnoremap <silent> gsr :SwitchReverse<CR>
nnoremap <silent> gsc :call switch#Switch(g:switch_defs_camel_pascal, [])<CR>
nnoremap <silent> gsq :call switch#Switch(g:switch_def_quote, [])<CR>
" }}}

" @submode {{{
let g:submode_always_show_submode=1

call submode#enter_with('winsize', 'n', '', '<C-w>>', '<C-w>>')
call submode#enter_with('winsize', 'n', '', '<C-w><', '<C-w><')
call submode#enter_with('winsize', 'n', '', '<C-w>+', '<C-w>+')
call submode#enter_with('winsize', 'n', '', '<C-w>-', '<C-w>-')
call submode#map('winsize', 'n', '', '>', '<C-w>>')
call submode#map('winsize', 'n', '', '<', '<C-w><')
call submode#map('winsize', 'n', '', '+', '<C-w>+')
call submode#map('winsize', 'n', '', '-', '<C-w>-')

" }}}

" @gundo {{{
let g:gundo_width = 35
let g:gundo_preview_height = 20
let g:gundo_right = 1
let g:gundo_preview_bottom = 1
let g:gundo_help = 0
let g:gundo_prefer_python3 = 1
nnoremap <silent> <F2> :GundoToggle<CR>
" }}}

" @tagbar {{{
let g:tagbar_sort = 0
let g:tagbar_indent = 0
nnoremap <silent><F8> :TagbarOpen fj<CR>
nnoremap <silent><S-F8> :TagbarClose<CR>
nnoremap <silent><C-F8> :echo tagbar#currenttag('%s','','p')<CR>
" }}}

" @dirvish {{{
nmap - <Plug>(dirvish_up)
augroup au_dirvish
  autocmd!
  " autocmd VimEnter * if !argc() | Dirvish | endif
  if has('win32')
    if has('patch-8.0.0501')
      autocmd FileType dirvish nnoremap <buffer> <silent> e :exe '!start ' . getline('.')<CR>
    else
      autocmd FileType dirvish nnoremap <buffer> <silent> e :exe '!explorer ' . getline('.')<CR>
    endif
  endif
augroup END
" }}}

" @choosewin {{{
nmap gw <Plug>(choosewin)
let g:choosewin_overlay_enable = 1
let g:choosewin_overlay_clear_multibyte = 1
let g:choosewin_blink_on_land = 0
let g:choosewin_statusline_replace = 0
let g:choosewin_tabline_replace = 0
" }}}

" @syntastic {{{
let g:syntastic_mode_map = {
      \ "mode": "passive"}
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 2
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_vim_checkers = ['vint']
let g:syntastic_python_checkers = ["flake8"]
" }}}

" @shot-f {{{
" no blank
augroup plugin-shot-f-highlight
  autocmd!
  autocmd ColorScheme * highlight default ShotFGraph ctermfg=red ctermbg=NONE cterm=NONE guifg=red guibg=NONE gui=NONE
  autocmd ColorScheme * highlight default ShotFBlank ctermfg=NONE ctermbg=red cterm=NONE guifg=NONE guibg=red gui=NONE
augroup END
" }}}

" after {{{ 
colorscheme iceberg
" }}}

" local {{{
try
  source local.vimrc
catch /E484/
catch
  echomsg v:exception
endtry
" }}}

" vim:set ts=8 sts=2 sw=2 tw=0 noet fdm=marker cms=\"\ %s:
